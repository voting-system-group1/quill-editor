import React, { useMemo } from "react";
import { useRouter } from "next/router";
import { RoomProvider, useOthers, useMyPresence } from "../liveblocks.config";
import Cursor from "../components/Cursor";
import styles from "./index.module.css";

/**
 * This file shows how to add basic live cursors on your product.
 */

const COLORS = [
  "#E57373",
  "#9575CD",
  "#4FC3F7",
  "#81C784",
  "#FFF176",
  "#FF8A65",
  "#F06292",
  "#7986CB",
];
type Presence = {
    cursor: { x: number; y: number };
  };
  type Storage = {};
  type UserMeta = { id: string };
  type RoomEvent = {};
  
  const { room, leave } = client.enterRoom<
    Presence,
    Storage,
    UserMeta,
    RoomEvent
  >("my-room-id", { initialPresence: {} });
  
  // Call this to update the current user's Presence
  function updateCursorPosition({ x, y }) {
    room.updatePresence({ cursor: { x, y } });
  }
  
  const others = room.getOthers();
  
  // Run __renderCursor__ when any other connected user updates their presence
  const unsubscribe = room.subscribe("others", (others, event) => {
    for (const { id, presence } of others) {
      const { x, y } = presence.cursor;
      __renderCursor__(id, { x, y });
    }
  }
  
  // Handle events and rendering
  // ...